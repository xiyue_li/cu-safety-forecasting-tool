# C-U Safety Forecasting Tool
More than 3,300 crashes happened every year in Champaign County in the past five years. Transportation planners and engineers often find it difficult to list and prioritize transportation projects based on safety measures as the required data and tools are not readily available to conduct regular and comprehensive safety evaluations for the region. This project aims to develop a safety forecasting tool and assist transportation professionals to understand where traffic crash risk is high in the Champaign-Urbana, IL region.

# Research Question
This project requires a careful selection of the time/space scales for analysis, including an improved set of explanatory variables and/or unobserved heterogeneity effects, in order to derive at the most sensible results. This section details the research scope by answering the following research questions:  

**What's predicted?**  
- [x] Crash frequency: total number of crashes in a year  
- [ ] Crash rate per vehicle mile: total crashes / (AADT X section length)
- [ ] Crash risk: the likelihood of crash occurrence

**What's the time horizon?**   
- [x] One year  
- [ ] One month
- [ ] One week
- [ ] One day

**What's the spatial dimension?**   
- [x] Segments
- [ ] Segments & intersections
- [ ] x mile by x mile grid

These questions will formulate the goal of the project:  
- [x] Roadway segments with the highest risk of crashes
- [ ] Probability of a crash to occur on Philo Road in February
- [ ] Location of the next most likely crash in the next 24 hours

# Report Structure
This project is implemented in a set of Jupyter Notebooks
* Notebook 1_Introduction.ipynb provides project background, summarizes literature review findings, introduces research methodology, and presents project conclusions.
* Notebook 2_Data_Source.ipynb collects and organizes data needed for the project, including 6 years of traffic crash data (2013-2018), roadway geometry data, demographic data, as well as weather station data (if needed).
* Notebook 3_Exploratory_Crash_Data_Analysis.ipynb conducts exploratory analysis on the traffic crash data to understand temporal, spatial, and other patterns of traffic crashes in Champaign County. [The C-U Traffic Crash Dashboard](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard) was created to present the descriptive statistics of the crash data.
* Notebook 4_Data_Preprocessing.ipynb conducts data preprocessing to prepare the datasets for the models. This includes assigning crashes to roadway segments, negative sampling, creating training/testing datasets, etc.
* Notebook 5_Modeling.ipynb builds, tests, evaluates different machine learning models, including:
    * Stochastic Gradient Descent Classifier
    * Random Forest
    * Support Vector Machine
    * Extreme Gradient Boosting
    * Neural Network
* Notebook 6 selects the best performing model and predict future crash risks using traffic volume numbers generated from the Travel Demand Model. The crash risk map in the [The C-U Traffic Crash Dashboard](https://gitlab.com/ccrpc/c-u-traffic-crash-dashboard) presents the final predicted crash risks (currently using crash density to represent historic crash risks).
* Notebook 7 summarizes project findings.

## Author
This project is funded by the Illinois Department of Transportation and carried out by Shuake Wuzhati and Xiyue Li at the Champaign County Regional Planning Commission (CCRPC).

## License
This project is licensed under BSD 3-Clause License - see the LICENSE.md file for details.
