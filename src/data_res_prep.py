
# Import libraries
import pandas as pd
import numpy as ny
import geopandas as gpd
from geopandas import GeoDataFrame
from shapely.geometry import Point
import psycopg2
import qgis
import pickle
import requests
import json

base_tool_dir = "L:/Safety_Crash Data Analysis/"
base_file_dir = "L:/Safety Forecasting Tool/"
base_gis_dir  = "G:/CUUATS/Safety Forecasting Tool/"

def crash_data_cleaning (year): # Used in  2_Data_Source Section 2.1.1
    ### INPUT the shp file and make a copy of lat-long from shp file
    ### The lat-long from the crash text (crash info) file sometime do not match with the shp file.
    df_shp_crash=gpd.read_file(base_tool_dir+"Crash Data Cleaning Methodology/Input_Data/Champaign_County_"+str(year)+".dbf") ##Read Shapefile and save as pandas dataframe
    df_shp_ICN=df_shp_crash[['CASE_ID','XCOORD','YCOORD']] ##The X and Y coordinate from the shape file
    df_shp_ICN=df_shp_ICN.rename(columns={"CASE_ID":"ICN","XCOORD":"X_Coord","YCOORD":"Y_Coord"}) ##rename the attibutes
    df_shp_ICN['ICN']=pd.to_numeric(df_shp_ICN['ICN']) ## "ICN is in text format, converted to numeric format

    #### Asign if crash is in the MPA boundary
    geometry = [Point(xy) for xy in zip(df_shp_crash.XCOORD, df_shp_crash.YCOORD)]
    df_shp_crash = GeoDataFrame(df_shp_crash, crs="EPSG:3436", geometry=geometry)
    df_shp_crash.crs={'init':'EPSG:3436'}
    # df_shp_crash = df_shp_crash.to_crs("EPSG:3435")
    df_shp_crash = df_shp_crash.to_crs({'init':'EPSG:3435'})
    MPA=gpd.read_file(base_gis_dir+"Data/SafetyForecastingTool.gpkg",layer="MPA",driver="GPKG")
    MPA = GeoDataFrame(MPA, crs="EPSG:3435", geometry=MPA['geometry'])

    df_shp_crash['MPA']=df_shp_crash.within(MPA.loc[0, 'geometry'])

    df_shp_crash_vehicleType=df_shp_crash[['CASE_ID','VEH1_TYPE','VEH2_TYPE','VEH3_TYPE','VEH4_TYPE','MPA']]
    df_shp_crash_vehicleType['CASE_ID']=pd.to_numeric(df_shp_crash_vehicleType['CASE_ID'])

    #### INPUT the text files
    #### Define variables for the crash, person and vehicle level text files obtained from IDOT
    txt_crash=base_tool_dir+"Crash Data Cleaning Methodology/Input_Data/CrashExtract_"+str(year)+".txt"   ## crash text
    txt_per=base_tool_dir+"Crash Data Cleaning Methodology/Input_Data/PersonExtract_"+str(year)+".txt"    ## person text
    txt_veh=base_tool_dir+"Crash Data Cleaning Methodology/Input_Data/VehicleExtract_"+str(year)+".txt"   ## Veh text

    #### The text file does not have attribute names, the following step will define attribute names to the text file variables.
    txt_col_crash=base_tool_dir+"Crash Data Cleaning Methodology/Input_Data/CrashExtract_Columns.txt"
    txt_col_per=base_tool_dir+"Crash Data Cleaning Methodology/Input_Data/PersonExtract_Columns.txt"
    txt_col_veh=base_tool_dir+"Crash Data Cleaning Methodology/Input_Data/VehExtract_Columns.txt"

    col_crash=pd.read_csv(txt_col_crash,sep=",")
    col_per=pd.read_csv(txt_col_per,sep=",")
    col_veh=pd.read_csv(txt_col_veh,sep=",")

    col_crash_list=col_crash.columns.tolist()
    col_per_list=col_per.columns.tolist()
    col_veh_list=col_veh.columns.tolist()

    ### The dataframe of the text file referred as "info" with the attribute names are below
    CrashinfoData=pd.DataFrame(pd.read_csv(txt_crash,sep=",",header=None,names=col_crash_list))       ## crash info
    PerinfoData=pd.DataFrame(pd.read_csv(txt_per,sep=",",header=None,names=col_per_list))             ## Person info
    VehinfoData=pd.DataFrame(pd.read_csv(txt_veh,sep=",",header=None,names=col_veh_list))             ## Vehicle info

    #### Summarize the injuries from Person Data, found that injuries in the shapefile are not consstent with the person data
    per_pivot=PerinfoData[['ICN','PersonInjuryClass']]
    df_person_pivot=per_pivot.pivot_table(index='ICN', columns='PersonInjuryClass',aggfunc=len,fill_value=0)
    df_PerInjuries=df_person_pivot.reset_index()
    df_PerInjuries_join=df_PerInjuries.rename(columns={0.0:"No_Injuries",1.0:"C_Injuries",2.0:"B_Injuries",3.0:"A_Injuries",4.0:"Fatalities"})

    #### Join the lat-long from the crash shp file to crash info file
    CrashInfo_XYJoin=CrashinfoData.join(df_shp_ICN.set_index('ICN'), on='ICN')

    ## Identify crash data points with no gelocation information based on X_Coord and Y_Coord joined from shapefile

    Gelocated=pd.DataFrame([])
    for i in range(0, len(CrashInfo_XYJoin.index)):
        if CrashInfo_XYJoin['X_Coord'].iat[i]>0 or CrashInfo_XYJoin['Y_Coord'].iat[i]>0:
            geolocated="Yes"
            ICN_1=CrashInfo_XYJoin['ICN'].iat[i]
            Gelocated=Gelocated.append(pd.DataFrame({'ICN_1':ICN_1,'Geolocated':geolocated},index=[0]),ignore_index=True)
        else:
            geolocated="No"
            Gelocated=Gelocated.append(pd.DataFrame({'ICN_1':ICN_1,'Geolocated':geolocated},index=[0]),ignore_index=True)
            i=i+1

    CrashInfo_Geolocated=pd.concat([CrashInfo_XYJoin,Gelocated],axis=1,sort=False)

    j=0
    for i in range(0, len(CrashInfo_XYJoin.index)):
        if CrashInfo_XYJoin['TSCrashCoordinateX'].iat[i]==0 or CrashInfo_XYJoin['TSCrashCoordinateY'].iat[i]==0 or CrashInfo_XYJoin['TSCrashLat'].iat[i]==0  or CrashInfo_XYJoin['TSCrashLong'].iat[i]==0:
            j=j+1
        else:
            i=i+1
    k=len(CrashInfo_XYJoin.index)-j
    print("No of crashes in the crash info file is "+ str(len(CrashInfo_XYJoin.index)))
    print("No of crashes in the crash info file with incomplete geolocation data "+ str(j))
    print("No of crashes with complete data from crash info file " + str(k))


    l=CrashInfo_Geolocated['Geolocated'].value_counts() ## No of Data points with no geolocation information
    print("No of crashes in the crash shape file is "+ str(l['Yes'])+'\n')

    if k==l['Yes']:
        print("The number of crashes with complete geolocation in shp and info file matches")
    else:
        print("Further analysis needs to be done and based on the judgement, either crash info or shp data can be to be used")
    ## Join the number of injuries by severity type estimated from person info file to the crash info file after cross checking the geolocation information
    CrashInfo_PerInj=CrashInfo_Geolocated.join(df_PerInjuries_join.set_index('ICN'), on='ICN')
    # Join vehicle type information from the crash dbf file
    CrashInfo_PerInj_Vehi=CrashInfo_PerInj.merge(df_shp_crash_vehicleType, left_on='ICN',right_on='CASE_ID')

    ## Export the crash info file to csv to be uploaded in QGIS
    CrashInfo_PerInj_Vehi.to_csv(path_or_buf=base_tool_dir+"Crash Data Cleaning Methodology/Output_Data/CrashInfo_PerInj_Vehi_"+str(year)+".csv")
    print('CrashInfo_PerInj_Vehi file for {} is saved'.format(year)+'\n')
    print("Next Steps are using the QGIS")

    return CrashInfo_PerInj_Vehi

def crash_data_merging(Train_start_yr,Train_end_yr,Test_start_yr,Test_end_yr,Predict_start_yr,Predict_end_yr,train_crashes_dataname,test_crashes_dataname):  # Used in 2_Data_Source Section 2.1.2
    # There are 90 columns in the crash file, the majority of which are not crucial for the purpose of this project.
    crashes_columns=['ICN','CrashYear','CrashMonth', 'CrashDay', 'CrashHour', 'DayofWeek',
                 'TypeofFirstCrash','TotalFatals', 'TotalInjured', 'NoInjuries', 'Ainjuries', 'Binjuries', 'Cinjuries', 'CrashInjuriesSeverity',
                 'NumberofVehicles','Cause1', 'Cause2',
                 'RoadwayFunctionalClass', 'RouteNumbers', 'ClassofTrafficway',  'NHS',
                 'RoadSurfaceCond','RoadDefects', 'LightCOndition', 'WeatherCondition',
                 'NumberofLanes', 'RoadAlignment', 'TrafficwayDescrip',
                 'TrafficCOntrolDevice', 'trafficControlDeviceCondition',
                 'VEH1_TYPE','VEH2_TYPE','VEH3_TYPE','VEH4_TYPE',
                 'WorkZoneRelated', 'CityTownshipFlag','CityName',
                 'IntersectionRelated', 'HitAndRun', 'RailroadCrossingNumber',
                 'TSCrashCoordinateX', 'TSCrashCoordinateY', 'TSCrashLat', 'TSCrashLong','MPA']

    mergedata_dir =  'Crash Data Cleaning Methodology/Output_Data/'
    filenames = [base_tool_dir+mergedata_dir+'CrashInfo_PerInj_Vehi_2013.csv',
    base_tool_dir+mergedata_dir+'CrashInfo_PerInj_Vehi_2014.csv',
    base_tool_dir+mergedata_dir+'CrashInfo_PerInj_Vehi_2015.csv',
    base_tool_dir+mergedata_dir+'CrashInfo_PerInj_Vehi_2016.csv',
    base_tool_dir+mergedata_dir+'CrashInfo_PerInj_Vehi_2017.csv',
    base_tool_dir+mergedata_dir+'CrashInfo_PerInj_Vehi_2018.csv']

    allcrashes = pd.DataFrame()
    for f in filenames:
        crash_yeari = pd.read_csv(f)
        crash_yeari = crash_yeari[crashes_columns]
        allcrashes = allcrashes.append(crash_yeari)
    allcrashes = allcrashes.reset_index()
    crash_columns_rename={'VEH1_TYPE': 'Vehicle_1_Type',
                      'VEH2_TYPE': 'Vehicle_2_Type',
                      'VEH3_TYPE': 'Vehicle_3_Type',
                      'VEH4_TYPE': 'Vehicle_4_Type',
                      'TrafficCOntrolDevice':'TrafficControlDevice',
                      'trafficControlDeviceCondition':'TrafficControlDeviceCondition',
                      'LightCOndition':'LightCondition'}
    allcrashes=allcrashes.rename(columns=crash_columns_rename)
    allcrashes['heavy']=0
    for i in range (0,allcrashes.shape[0]):
        if sum(pd.Series([allcrashes.loc[i,'Vehicle_1_Type'],
                      allcrashes.loc[i,'Vehicle_2_Type'],
                      allcrashes.loc[i,'Vehicle_3_Type'],
                      allcrashes.loc[i,'Vehicle_4_Type']]).str.contains('Tractor|Truck|Bus|Farm',na=False))>0:
            allcrashes.loc[i,'heavy']=1
    allcrashes['TSCrashLong'] = -abs(allcrashes['TSCrashLong'])
    print('There are',allcrashes.shape[0],'crashes from year',min(allcrashes['CrashYear']),'to year',max(allcrashes['CrashYear']),'in total'+'\n')
    print('Crash counts by year:')
    print(allcrashes.groupby(['CrashYear','heavy']).size())
    print('\n')

    crashdir = 'data/interim/crash/'

    train_crashes = allcrashes[(allcrashes['CrashYear']>=Train_start_yr)&(allcrashes['CrashYear']<=Train_end_yr)]
    train_crashes = train_crashes.reset_index(drop=True)
    test_crashes = allcrashes[(allcrashes['CrashYear']>=Test_start_yr)&(allcrashes['CrashYear']<=Test_end_yr)]
    test_crashes = test_crashes.reset_index(drop=True)

    # Save data
    train_crashes.to_csv(base_file_dir+crashdir+train_crashes_dataname,index=False)
    print('Geometry bound of the train crahes:')
    print (train_crashes['TSCrashLong'].max())
    print (train_crashes['TSCrashLong'].min())
    print (train_crashes['TSCrashLat'].max())
    print (train_crashes['TSCrashLat'].min())
    print('train crash data saved in {}'.format(train_crashes_dataname)+'\n')

    test_crashes.to_csv(base_file_dir+crashdir+test_crashes_dataname,index=False)
    print('Geometry bound of the test crahes:')
    print (test_crashes['TSCrashLong'].max())
    print (test_crashes['TSCrashLong'].min())
    print (test_crashes['TSCrashLat'].max())
    print (test_crashes['TSCrashLat'].min())
    print('test crash data saved in {}'.format(test_crashes_dataname))

def ReadLayer (year):  # Used in 2_Data_Source Section 2.2.1
    geopackage=base_gis_dir+"Data/IDOT_HWY"+str(year)+".gpkg"
    layer="IDOT_HWY"+str(year)
    return gpd.read_file(geopackage, driver="GPKG", layer=layer)

def IDOT_Highway_org():  # Used in 2_Data_Source Section 2.2.1
    # Layers for study
    HWY_2018 = ReadLayer (2018)
    HWY_2017 = ReadLayer (2017)
    HWY_2016 = ReadLayer (2016)
    HWY_2015 = ReadLayer (2015)
    HWY_2014 = ReadLayer (2014)
    # Historic layers needed for feature extrapolation
    HWY_2013 = ReadLayer (2013)
    HWY_2012 = ReadLayer (2012)
    HWY_2011 = ReadLayer (2011)
    HWY_2010 = ReadLayer (2010)
    HWY_2009 = ReadLayer (2009)

    HWY_2017=HWY_2017.rename(columns={"FC_NAME": "FCNAME"})

    HWY_attri=['INVENTORY','BEG_STA','END_STA','AADT','AADT_YR','HCV','HCV_MU_YR','MU_VOL','SU_VOL',
                       'ACC_CNTL','I_SHD1_TYP','I_SHD1_WTH','I_SHD2_TYP','I_SHD2_WTH','LN_WTH','LNS','MED_TYP',
                       'MED_WTH','SURF_TYP','SURF_WTH','O_SHD1_TYP','O_SHD1_WTH','O_SHD2_TYP','O_SHD2_WTH','OP_1_2_WAY',
                       'PRK_LT','PRK_RT','SEG_LENGTH','Shape_Leng','CRS_LOW','CRS_YR','DTRESS_OPP','FAULT_LOW','IRI_LOW',
                       'RUT_LOW','SURF_YR','FCNAME','JUR_TYPE','MPO','MUNI_NAME','NHS','ROAD_NAME','SP_LIM','TRK_RT']

    HWY_2018=HWY_2018[HWY_attri]
    HWY_2017=HWY_2017[HWY_attri]
    HWY_2016=HWY_2016[HWY_attri]
    HWY_2015=HWY_2015[HWY_attri]
    HWY_2014=HWY_2014[HWY_attri]

    #Historic IDOT highway networks does not have pavement condition information
    HWY_his_attri=['INVENTORY','BEG_STA','END_STA','AADT','AADT_YR','HCV','HCV_MU_YR','FCNAME']
    HWY_2013=HWY_2013[HWY_his_attri]
    HWY_2012=HWY_2012[HWY_his_attri]
    HWY_2011=HWY_2011[HWY_his_attri]
    HWY_2010=HWY_2010[HWY_his_attri]
    HWY_2009=HWY_2009[HWY_his_attri]

    print('Write IDOT Highway information to HWY_2009.pkl~HWY_2018.pkl')
    with open(base_file_dir+'data/interim/roadway/data_source/HWY_2018.pkl', 'wb') as f:
        pickle.dump(HWY_2018, f)
    with open(base_file_dir+'data/interim/roadway/data_source/HWY_2017.pkl', 'wb') as f:
        pickle.dump(HWY_2017, f)
    with open(base_file_dir+'data/interim/roadway/data_source/HWY_2016.pkl', 'wb') as f:
        pickle.dump(HWY_2016, f)
    with open(base_file_dir+'data/interim/roadway/data_source/HWY_2015.pkl', 'wb') as f:
        pickle.dump(HWY_2015, f)
    with open(base_file_dir+'data/interim/roadway/data_source/HWY_2014.pkl', 'wb') as f:
        pickle.dump(HWY_2014, f)
    with open(base_file_dir+'data/interim/roadway/data_source/HWY_2013.pkl', 'wb') as f:
        pickle.dump(HWY_2013, f)
    with open(base_file_dir+'data/interim/roadway/data_source/HWY_2012.pkl', 'wb') as f:
        pickle.dump(HWY_2012, f)
    with open(base_file_dir+'data/interim/roadway/data_source/HWY_2011.pkl', 'wb') as f:
        pickle.dump(HWY_2011, f)
    with open(base_file_dir+'data/interim/roadway/data_source/HWY_2010.pkl', 'wb') as f:
        pickle.dump(HWY_2010, f)
    with open(base_file_dir+'data/interim/roadway/data_source/HWY_2009.pkl', 'wb') as f:
        pickle.dump(HWY_2009, f)

def retrieve_pop(API): # Used in 2_Data_Source Section 2.3
    print('This function Retrieve total popualation by block group information from U.S. Census Bureau ACS from year 2014 to 2018'+'\n')

    # with open('Census_API.txt', 'r') as file:
    # API = file.read().replace('\n', '')

    response_2014 = requests.get('https://api.census.gov/data/2014/acs/acs5?get=NAME,B00001_001E&for=block%20group:*&in=state:17+county:019&key='+API)
    response_2015 = requests.get('https://api.census.gov/data/2015/acs/acs5?get=NAME,B00001_001E&for=block%20group:*&in=state:17+county:019&key='+API)
    response_2016 = requests.get('https://api.census.gov/data/2016/acs/acs5?get=NAME,B00001_001E&for=block%20group:*&in=state:17+county:019&key='+API)
    response_2017 = requests.get('https://api.census.gov/data/2017/acs/acs5?get=NAME,B00001_001E&for=block%20group:*&in=state:17+county:019&key='+API)
    response_2018 = requests.get('https://api.census.gov/data/2018/acs/acs5?get=NAME,B00001_001E&for=block%20group:*&in=state:17+county:019&key='+API)

    #load the response into a JSON, ignoring the first element which is just field labels
    response_2014_js = json.loads(response_2014.text)[1:]
    response_2015_js = json.loads(response_2015.text)[1:]
    response_2016_js = json.loads(response_2016.text)[1:]
    response_2017_js = json.loads(response_2017.text)[1:]
    response_2018_js = json.loads(response_2018.text)[1:]
    #store the response in a dataframe
    pop_2014 = pd.DataFrame(columns=["NAME","Pop_2014","state","county","tract","block group"],data=response_2014_js)
    pop_2015 = pd.DataFrame(columns=["NAME","Pop_2015","state","county","tract","block group"],data=response_2015_js)
    pop_2016 = pd.DataFrame(columns=["NAME","Pop_2016","state","county","tract","block group"],data=response_2016_js)
    pop_2017 = pd.DataFrame(columns=["NAME","Pop_2017","state","county","tract","block group"],data=response_2017_js)
    pop_2018 = pd.DataFrame(columns=["NAME","Pop_2018","state","county","tract","block group"],data=response_2018_js)

    pop_2014 = pop_2014.astype({'Pop_2014': 'int32'})
    pop_2015 = pop_2015.astype({'Pop_2015': 'int32'})
    pop_2016 = pop_2016.astype({'Pop_2016': 'int32'})
    pop_2017['Pop_2017'] = pd.to_numeric(pop_2017['Pop_2017'], errors='coerce')
    pop_2018['Pop_2018'] = pd.to_numeric(pop_2018['Pop_2018'], errors='coerce')

    pop = pd.merge(pop_2014, pop_2015[['NAME','Pop_2015']],
             how='left',
             on='NAME')
    pop = pd.merge(pop, pop_2016[['NAME','Pop_2016']],
             how='left',
             on='NAME')
    pop = pd.merge(pop, pop_2017[['NAME','Pop_2017']],
             how='left',
             on='NAME')
    pop = pd.merge(pop, pop_2018[['NAME','Pop_2018']],
             how='left',
             on='NAME')
    pop['GEOID'] = pop['state']+pop['county']+pop['tract']+pop['block group']

    return pop

def pop_density(pop): # Used in 2_Data_Source Section 2.3
    blockgroup_geopackage=base_gis_dir+"Data/Blockgroups.gpkg"
    blockgroups=gpd.read_file(blockgroup_geopackage, driver="GPKG", layer='Blockgroups')
    blockgroups = blockgroups.set_geometry('geometry')
    blockgroups.crs = "EPSG:3435"
    blockgroups["area_sqm"] = blockgroups['geometry'].area*3.587e-8
    bg_pop=pd.merge(blockgroups[['GEOID','geometry','area_sqm']], pop[['GEOID','Pop_2014','Pop_2015','Pop_2016','Pop_2017','Pop_2018']],
             how='left',
             on='GEOID')
    bg_pop['Pop_den_2014']=bg_pop['Pop_2014']/bg_pop['area_sqm']
    bg_pop['Pop_den_2015']=bg_pop['Pop_2015']/bg_pop['area_sqm']
    bg_pop['Pop_den_2016']=bg_pop['Pop_2016']/bg_pop['area_sqm']
    bg_pop['Pop_den_2017']=bg_pop['Pop_2017']/bg_pop['area_sqm']
    bg_pop['Pop_den_2018']=bg_pop['Pop_2018']/bg_pop['area_sqm']

    return bg_pop
