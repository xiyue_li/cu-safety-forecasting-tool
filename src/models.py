import numpy as np
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import cross_val_score
from matplotlib import pyplot as plt 
import warnings
import pandas as pd
import geopandas as gpd
import datetime
from csv import DictWriter
from sklearn.compose import ColumnTransformer
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import FunctionTransformer
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import StratifiedShuffleSplit
from imblearn.over_sampling import SMOTE
from imblearn.combine import SMOTEENN
from imblearn.pipeline import Pipeline
from sklearn.metrics import precision_recall_curve


# 5.2. 1) Explanatory variable subsets -- Remove list of variables
def func_remove_var (df,remove_list):
    variables=np.array(df.columns.tolist())
    remove_index=[]
    variable_index=list(range(len(variables)))
    for i in range(len(remove_list)):
        remove_index.append(np.where(variables == remove_list[i])[0][0])
    df_sub=df.drop(df.columns[remove_index],axis=1,inplace=False)
    return df_sub

# 5.2. 1) Explanatory variable subsets -- Keep list of variables
def func_keep_var (df,keep_list):
    variables=np.array(df.columns.tolist())
    keep_index=[]
    for i in range(len(keep_list)):
        keep_index.append(np.where(variables == keep_list[i])[0][0])
    return df.iloc[:,keep_index]

# 5.2. 3) Feature Preprocessing -- transform numerical and categorical variables 
def func_transform_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()[1:]
    cat_attributes=[x for x in df_variables if x not in num_attributes][1:-1]
    variables_pipeline = ColumnTransformer([
        ("num", StandardScaler(), num_attributes),
        ("cat", OneHotEncoder(), cat_attributes),
    ])
    df_transformed = variables_pipeline.fit_transform(df)
    return df_transformed

# 5.2. 3) Feature Preprocessing -- get the variable names of the transformed training data
def func_transformed_var (df):
    df_variables=df.columns.tolist()
    num_attributes=df.describe().T.index.tolist()[1:]
    cat_attributes=[x for x in df_variables if x not in num_attributes][1:-1]
    num_feature_names = num_attributes
    cat_feature_names = pd.get_dummies(df[cat_attributes],columns=cat_attributes).columns.tolist()
    df_feature_names = num_feature_names+cat_feature_names
    feature_sel = range(len(df_feature_names))
    fnames = np.array(df_feature_names)[feature_sel]
    return fnames

def func_regression_pipeline (model):
    return (Pipeline(
    steps=[
        ('variable_transformer', FunctionTransformer(func_transform_var, validate=False)),
        ('model', model)
    ], verbose=True))

def func_classfication_pipeline (model, sampling):
    return (Pipeline(
    steps=[
        ('variable_transformer', FunctionTransformer(func_transform_var, validate=False)),
        ('sampling',sampling),
        ('model', model)
    ], verbose=True))

# For regression models, merge all crash cases with less than 2 cases to one and sample the training data for quick test of models
def func_Shuffle_num (X, y, train_size):
    crash_count=y.value_counts()
    try:
        update_crashes=crash_count[crash_count == 2].index[0]
        y_upate=y.mask(y >= update_crashes, update_crashes-1)
    except:
        y_update=y
    split=StratifiedShuffleSplit(n_splits=1, train_size=train_size, random_state=42)
    for train_index, test_index in split.split(X,y_update):
        X_sample=X.iloc[train_index]
        y_sample=y_update.iloc[train_index].to_numpy()
    return X_sample, y_sample

# For classification models, convert lable crash count to 4 crash categories and sample the training data for quick test of models
def func_Shuffle_cat (X, y, train_size):
    y_cat = y.mask(y >= 4, 3)
    y_cat_label=LabelEncoder()
    y_cat=y_cat_label.fit_transform(y_cat)
    split=StratifiedShuffleSplit(n_splits=1, train_size=train_size, random_state=42)
    for train_index, test_index in split.split(X,y_cat):
        X_sample=X.iloc[train_index]
        y_sample=y_cat[train_index]
    return X_sample, y_sample

# For simple crash vs. no crash classification models, merge all non-zero crash count to 1 category and sample the training data for quick test of models
def func_Shuffle_simple_cat (X, y, train_size):
    y_cat = y.mask(y >= 1, 1)
    y_cat_label=LabelEncoder()
    y_cat=y_cat_label.fit_transform(y_cat)
    split=StratifiedShuffleSplit(n_splits=1, train_size=train_size, random_state=42)
    for train_index, test_index in split.split(X,y_cat):
        X_sample=X.iloc[train_index]
        y_sample=y_cat[train_index]
    return X_sample, y_sample


# plot no skill and model precision-recall curves
def plot_pr_curve (test_y, model_probs):
    # calculate the no skill line as the proportion of the positive class
    no_skill = len(test_y[test_y==1]) / len(test_y)
    # plot the no skill precision-recall curve
    plt.plot([0, 1], [no_skill, no_skill], linestyle='--', label='No Skill')
    # plot model precision-recall curve
    precision, recall, _ = precision_recall_curve(test_y, model_probs)
    plt.plot(recall, precision, marker='.', label='Logistic')
    # axis labels
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    # show the legend
    plt.legend()
    # show the plot
    plt.show()
    
    

# 5.3. 1) Overall Mean Squared Error
def func_rmse (label,prediction):
    mse = mean_squared_error(label, prediction)
    rmse = np.sqrt(mse)
    return rmse

# 5.3. 2) Cross Valuation Score
def func_cross_val (model, df, label, cv=2, scoring="neg_mean_squared_error"): # scoring can be changed, esp. for classification problem
    model_scoresx=cross_val_score(model, df, label, cv=cv, scoring=scoring)
    return model_scoresx #("%0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))

# 5.3.3) Mean Squared Error by number of crashes
def func_rmse_crashes (label,prediction):
    prediction=pd.DataFrame(prediction)
    # rmse for 0 observed crashes
    label_0=label[label==0].reset_index(drop=True)
    label_0_index=label_0.index.tolist()
    prediction_0=prediction.iloc[label_0_index,0]
    mse_0 = mean_squared_error(label_0, prediction_0)
    rmse_0 = np.sqrt(mse_0)
    # rmse for 1 observed crashes
    label_1=label[label==1].reset_index(drop=True)
    label_1_index=label_1.index.tolist()
    prediction_1=prediction.iloc[label_1_index,0]
    mse_1 = mean_squared_error(label_1, prediction_1)
    rmse_1 = np.sqrt(mse_1)
    # rmse for 2 observed crashes
    label_2=label[label==2].reset_index(drop=True)
    label_2_index=label_2.index.tolist()
    prediction_2=prediction.iloc[label_2_index,0]
    mse_2 = mean_squared_error(label_2, prediction_2)
    rmse_2 = np.sqrt(mse_2)
    # rmse for 3 observed crashes
    label_3=label[label==3].reset_index(drop=True)
    label_3_index=label_3.index.tolist()
    prediction_3=prediction.iloc[label_3_index,0]
    mse_3 = mean_squared_error(label_3, prediction_3)
    rmse_3 = np.sqrt(mse_3)
    # rmse for more than 3 observed crashes
    label_4=label[label>=4].reset_index(drop=True)
    label_4_index=label_4.index.tolist()
    prediction_4=prediction.iloc[label_4_index,0]
    mse_4 = mean_squared_error(label_4, prediction_4)
    rmse_4 = np.sqrt(mse_4)
    return [rmse_0, rmse_1, rmse_2, rmse_3, rmse_4]

# 5.3.4) Error by functional class
def func_functional_compare(df_Func,label,prediction):
    # prepare data
    df_Func=pd.DataFrame.sparse.from_spmatrix(df_Func)
    df_Func.columns = ["Interstate", "Major Arterial", "Minor Arterial", "Major Collector", "Minor Collector","Local Road or Street"]
    df_Func['crashes']=label
    df_Func['modeled']=prediction
    df_Func=df_Func.fillna(0)
    # filter data by functioanl class
    Interstate=df_Func[df_Func['Interstate']==1]
    MajorArterial=df_Func[df_Func['Major Arterial']==1]
    MinorArterial=df_Func[df_Func['Minor Arterial']==1]
    MajorCollector=df_Func[df_Func['Major Collector']==1]
    MinorCollector=df_Func[df_Func['Minor Collector']==1]
    LocalRoad=df_Func[df_Func['Local Road or Street']==1]
    # get rmse by functional class
    rmse_interstate=np.sqrt(mean_squared_error(Interstate.crashes, Interstate.modeled))
    rmse_MajorArterial=np.sqrt(mean_squared_error(MajorArterial.crashes, MajorArterial.modeled))
    rmse_MinorArterial=np.sqrt(mean_squared_error(MinorArterial.crashes, MinorArterial.modeled))
    rmse_MajorCollector=np.sqrt(mean_squared_error(MajorCollector.crashes, MajorCollector.modeled))
    rmse_MinorCollector=np.sqrt(mean_squared_error(MinorCollector.crashes, MinorCollector.modeled))
    rmse_LocalRoad=np.sqrt(mean_squared_error(LocalRoad.crashes, LocalRoad.modeled))
    return [rmse_interstate, rmse_MajorArterial, rmse_MinorArterial, rmse_MajorCollector, rmse_MinorCollector, rmse_LocalRoad]


# 5.3.6) Histogram
def func_hist (label,prediction):
    histo_label=pd.DataFrame(label.value_counts()[0:3])
    histo_label.loc[4]=label.value_counts()[4:].sum()
    ranges = [-1,0,1,2,3,4,5,6,7,8,9,10]
    prediction_df=pd.DataFrame(prediction)
    prediction_df.columns=['modeled']
    modeled=prediction_df.modeled.groupby(pd.cut(prediction_df.modeled, ranges)).count()
    modeled=modeled.reset_index(drop=True)
    histo_modeled=pd.DataFrame(modeled[0:4])
    histo_modeled.loc[4]=modeled[4:].sum()
    histo_label['modeled']=histo_modeled.modeled
    histo_label['diff']=(histo_label['Crashes']-histo_label['modeled'])/histo_label['Crashes']
    return histo_label['diff']
    
# 5.3.7) Mapping -- Observed crashes mapping
def observed_crashes_map (df_geom,year):
    warnings.simplefilter(action='ignore', category=UserWarning)
    gdf_geom  = gpd.GeoDataFrame(df_geom, geometry=df_geom['geometry'])
    gdf_geom['Crashes_cat']=gdf_geom['Crashes']
    gdf_geom.loc[gdf_geom['Crashes']>3,'Crashes_cat']='4+'
    gdf_geom['Crashes_cat']=gdf_geom['Crashes_cat'].astype('category')
    gdf_geom['Crashes_cat']=gdf_geom['Crashes_cat'].apply(str)
    fig, ax = plt.subplots(1, 1, figsize=(15,15))
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='0.0')].plot(column='Crashes_cat',ax=ax, linewidth=0.5,legend=True,color='grey')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='1.0')].plot(column='Crashes_cat',ax=ax, linewidth=1,legend=True,color='orange')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='2.0')].plot(column='Crashes_cat',ax=ax, linewidth=2,legend=True,color='red')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='3.0')].plot(column='Crashes_cat',ax=ax, linewidth=3,legend=True,color='brown')
    gdf_geom[
        (gdf_geom['Year']==year)&
        (gdf_geom['Crashes_cat']=='4+')].plot(column='Crashes_cat',ax=ax, linewidth=4,legend=True,color='black')
    fig.savefig('../reports/figures/observed_crashes/observed_crashes_'+str(year)+'.png')
    
# 5.3.7) Mapping -- # Trainig set modeled crashes mapping
def prediction_maps (df_geom, prediction, year, segment_intersection, model_geography, model_name, map_name):
    prediction_geom = df_geom.merge(pd.Series(prediction,name='prediction').to_frame(),  left_index=True, right_index=True, how='right')
    prediction_geom  = gpd.GeoDataFrame(prediction_geom , geometry=prediction_geom['geometry'])
    warnings.simplefilter(action='ignore', category=UserWarning)
    fig, ax = plt.subplots(1, 1, figsize=(15,15))
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']<=0)].plot(column='prediction',ax=ax, linewidth=0.5,legend=True,color='grey')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']>0)&
        (prediction_geom['prediction']<=1)].plot(column='Crashes_cat',ax=ax, linewidth=1,legend=True,color='orange')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']>1)&
        (prediction_geom['prediction']<=2)].plot(column='Crashes_cat',ax=ax, linewidth=2,legend=True,color='red')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']>2)&
        (prediction_geom['prediction']<=3)].plot(column='Crashes_cat',ax=ax, linewidth=3,legend=True,color='brown')
    prediction_geom[
        (prediction_geom['Year']==year)&
        (prediction_geom['prediction']>=4)].plot(column='Crashes_cat',ax=ax, linewidth=4,legend=True,color='black')
    fig.savefig('../reports/figures/model_results/'+segment_intersection+'/'+model_geography+'/'+model_name+'/'+str(year)+'_'+map_name+'.png')

# 5.3.7) Mapping --  Test set modeled crashes mapping
def test_prediction_maps (df_geom, prediction, segment_intersection, model_geography, model_name, map_name, start_index=979036):
    df_prediction=pd.Series(prediction,name='prediction').to_frame()
    df_prediction.index+= start_index
    prediction_geom = df_geom.merge(df_prediction, left_index=True, right_index=True, how='right')
    prediction_geom  = gpd.GeoDataFrame(prediction_geom , geometry=prediction_geom['geometry'])
    warnings.simplefilter(action='ignore', category=UserWarning)
    fig, ax = plt.subplots(1, 1, figsize=(15,15))
    prediction_geom[prediction_geom['prediction']<=0].plot(column='prediction',ax=ax, linewidth=0.5,legend=True,color='grey')
    prediction_geom[
        (prediction_geom['prediction']>0)&
        (prediction_geom['prediction']<=1)].plot(column='Crashes_cat',ax=ax, linewidth=1,legend=True,color='orange')
    prediction_geom[
        (prediction_geom['prediction']>1)&
        (prediction_geom['prediction']<=2)].plot(column='Crashes_cat',ax=ax, linewidth=2,legend=True,color='red')
    prediction_geom[
        (prediction_geom['prediction']>2)&
        (prediction_geom['prediction']<=3)].plot(column='Crashes_cat',ax=ax, linewidth=3,legend=True,color='brown')
    prediction_geom[prediction_geom['prediction']>=4].plot(column='Crashes_cat',ax=ax, linewidth=4,legend=True,color='black')
    fig.savefig('../reports/figures/model_results/'+segment_intersection+'/'+model_geography+'/'+model_name+'/'+'2018_'+map_name+'.png')
    
# 5.3.8) Summary report
def summary_report (reg, label, prediction, df, n, scoring, fnames):
    # overall rmse
    overall_rmse=func_rmse(label,prediction)
    # cross valuation score
    cross_val_score=func_cross_val(reg, df, label, n, scoring)
    # rmse by number of crashes
    crashes_rmse=func_rmse_crashes(label,prediction)
    # error by functional class
    keep_list=['functional_classification_Interstate',
           'functional_classification_Major Arterial',
           'functional_classification_Minor Arterial',
           'functional_classification_Major Collector',
           'functional_classification_Minor Collector',
           'functional_classification_Local Road or Street',]
    df_Func=func_keep_var(df,keep_list,fnames)
    func_error=func_functional_compare(df_Func,label,prediction)
    # histogram
    histo=func_hist(label,prediction)
    return [overall_rmse, cross_val_score, crashes_rmse, func_error, histo]

def model_log (file_name, dict_of_elem, field_names):
    with open (file_name, 'a+', newline='') as write_obj:
        dict_writer = DictWriter (write_obj, fieldnames=field_names)
        dict_writer.writerow (dict_of_elem)